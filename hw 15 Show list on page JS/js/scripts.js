


//    Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM)
//    ----------------------------------------------------------------------------------
//    DOM -  это объектная модель документа, которая позволяет получать доступ к HTML, XHTML и XML документам,
//     а так же изменять содержимое, структуру, и оформленлени.
function getList () {
    let arr = ['hello world','text','some text', 'str', 19];
    const arrMap = `<ul> ${arr.map(list => `<li> ${list}</li>`)} </ul>`;
    document.getElementById('container').innerHTML = arrMap.split(',').join(' ');

    //timer
    let seconds = 10;
    function timeSec() {
        if (seconds < 1) {
            document.body.innerHTML = '';
        } else {
            document.getElementById('timer').innerHTML = seconds;
            setTimeout(timeSec, 1000);
            seconds--;
        }
    }

    timeSec();
}
getList()