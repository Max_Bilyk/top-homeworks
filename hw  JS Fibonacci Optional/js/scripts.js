let firstNum = +prompt('Please, enter first number');
let secondNum = +prompt('Please enter second number');
let fibNum = +prompt('Enter number for Fibonacci');
while(isNaN(firstNum) || firstNum === "") {
    firstNum = +prompt('Enter first number again, please use only numbers');
}
while(isNaN(secondNum || secondNum === "")) {
    secondNum = +prompt('Enter second number again, please use only numbers');
}
while(isNaN(fibNum) || fibNum === "") {
    fibNum = +prompt('Enter number for Fibonacci again, use only numbers');
}
function fibNumberPlus (fibNumber, firstNumber,secondNumber) {
        let f1 = firstNumber, f2 = secondNumber, f3;
        for (let i = 2; i < fibNumber; i++) {
             f3 = f1 + f2;
             f1 = f2;
             f2 = f3;
        }
        return f3;
}
alert(`Value of Fibonacci --> ${fibNumberPlus(fibNum, firstNum, secondNum)}`);
