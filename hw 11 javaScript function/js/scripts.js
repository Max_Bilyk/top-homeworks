const calcName = confirm('Do you want to use calculator');
if (calcName === true){
    let firstNumber;
    let secondNumber;
    let result;

    do {
        firstNumber = +prompt('Enter first number');
    }while (isNaN(firstNumber) || firstNumber === '' || firstNumber === 0);

    do {
        secondNumber = +prompt('Enter second number');
    }while (isNaN(secondNumber) || secondNumber === '' || secondNumber === 0);

    const operationPrompt = prompt('Choose operation', '+, -, *, /');

    function calculator(firstNum, secondNum, operation) {
        switch (operation) {
            case '+':
                result = firstNum + secondNum;
                break;

            case  '-':
                result = firstNum - secondNum;
                break;

            case '*':
                result = firstNum * secondNum;
                break;

            case '/':
                result = firstNum / secondNum;
                break;

            default:
                alert('There is no such action');
                result = 0;
        }
        return result
    }

    alert('Result --->' + calculator(firstNumber, secondNumber, operationPrompt));
}else {
    alert('Canceled')
}
