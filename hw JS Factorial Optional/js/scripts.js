let userNum = +prompt('Enter number for factorial');
while (userNum < 0 || isNaN(userNum) || userNum === 0) {
    userNum = +prompt('Please, enter number for factorial, use only numbers');
}
function factorial(userNumber) {
    return (userNumber !== 1) ? userNumber * factorial(userNumber - 1) : 1;
}
alert(`Result of Factorial number ${userNum} ---> ${factorial(userNum)}`);