$(document).ready(function() {
    console.log('Document is ready to use!!!');
    let htmlWithBody = $('html, body');

    $('.page-navigation > .navigation-menu > .menu__item > .item__link').click(function (e) {
        e.preventDefault();

        let linkElement = $(this).attr('href'); // Хранит текущий href
        let distance = $(linkElement).offset().top; // Положение того или иного блока относительно верха ерана (px)
        console.log(linkElement);
        console.log(distance);

        htmlWithBody.animate({
            scrollTop: distance,
        }, 700);
    });

    $(window).scroll(function () {
        if ($(window).scrollTop() > 550){
            $('.back-btn').css('opacity', '1');
        }else {
            $('.back-btn').css('opacity', '0');
        }
    });

    $('.back-btn').click(function (e) {
        e.preventDefault();

        htmlWithBody.animate({
            scrollTop: 0,
        }, 700)
    });

    $('.show-hide').click(function (){
        $('.gallery__wrapper').slideToggle('slow')
    })
});
