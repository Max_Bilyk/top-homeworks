$(document).ready(function () {

    //CHECKBOX
    $.each($('.checkbox'), function (index, value){
        if($(this).find('input').prop('checked') === true){
            $(this).addClass('active');
        }
    });
    $(document).on('click', '.checkbox', function (e) {
        if ($(this).hasClass('active')){
            $(this).find('input').prop('checked', false);
        }else {
            $(this).find('input').prop('checked', true);
        }
        $(this).toggleClass('active');
    });

    //radio-buttons
    $.each($('.radio-buttons__item'), function (index, val){
        if ($(this).find('input').prop('checked') === 'true'){
            $(this).addClass('active');
        }
    });
    $(document).on('click', '.radio-buttons__item', function (e){
        $(this).parents('.radio-buttons').find('.radio-buttons__item').removeClass('active');
        $(this).parents('.radio-buttons').find('.radio-buttons__item input').prop('checked', false);
        $(this).toggleClass('active');
        $(this).find('input').prop('checked', true);
        return false;
    })
});