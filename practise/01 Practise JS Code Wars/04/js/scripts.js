const array1 = [10, 20, 30, 50];
const array2 = [90, 50, 30, 10]
function arrayPlusArray(arr1, arr2) {
    return arr1.concat(arr2).reduce((a, b) => a + b);
}
console.log(arrayPlusArray(array1, array2));

let arr = [5];