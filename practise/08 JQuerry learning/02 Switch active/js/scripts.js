// const buttons = document.getElementsByClassName("btn-danger");
//
// for(let i = 0; i < buttons.length; i++) {
//     buttons[i].addEventListener("click", toggleActive);
// }
//
// function toggleActive() {
//     const buttons = document.getElementsByClassName("btn-danger");
//
//     for(let i = 0; i < buttons.length; i++) {
//         buttons[i].classList.remove("active");
//     }
//
//     this.classList.toggle("active");
// }
$(".btn-danger").click(function() { // вешаем обработчик события в виде анонимной функции на все элементы с классом btn-danger
    $(".btn-danger").removeClass("active"); // удаляем у всех элементов с этим классом клас active
    $(this).addClass("active"); // добавляем его текущему элементу
});