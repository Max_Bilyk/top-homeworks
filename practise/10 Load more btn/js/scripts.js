$(document).ready(function () {
    let div = $('.content')
    $(div).slice(0, 12).show();
    $('#loadMore').click(function (e) {
        e.preventDefault();
        $('div:hidden').slice(0, 12).slideDown();
        if ($(div) > 12) {
            $('button').attr('disabled', true);
        }
    })
});