//1.В папке tabs лежит разметка для вкладок. Нужно, чтобы по нажатию на вкладку отображался конкретный текст для нужной вкладки.
// При этом остальной текст должен быть скрыт. В комментариях указано, какой текст должен отображаться для какой вкладки.
// 2.Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.
// 3.Нужно предусмотреть, что текст на вкладках может меняться, и что вкладки могут добавляться и удаляться.
// При этом нужно, чтобы функция, написанная в джаваскрипте, из-за таких правок не переставала работать.
function tab (){
    //Табы навигации
    let tabOfNav = document.querySelectorAll('.tabs-nav-item');
    //Табы с контентом
    let tabWithContent = document.querySelectorAll('.tab');
    //Переменная для записи активного таба, на котороый мы кликнули
    let tabName;

    tabOfNav.forEach(item => {
        item.addEventListener('click', selectTabOfNav)
    });
    //Добавляем и убираем класс - 'is-active'
    function selectTabOfNav(){
        tabOfNav.forEach(item => {
            item.classList.remove('is-active');
        });
        this.classList.add('is-active');
        tabName = this.getAttribute('data-tab-name');
        selectTabContent(tabName);
        // console.log(tabName)
    }
    function selectTabContent(tabName){
        tabWithContent.forEach(item => {
            item.classList.contains(tabName) ? item .classList.add('is-active') : item.classList.remove('is-active')
        })
    }
}
tab();