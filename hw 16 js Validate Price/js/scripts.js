//При загрузке страницы показать пользователю поле ввода (input) с надписью Price. Это поле будет служить для ввода числовых значений
// Поведение поля должно быть следующим:
//
// При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. При потере фокуса она пропадает.
// Когда убран фокус с поля - его значение считывается, над полем создается span, в котором должен быть выведен текст: Текущая цена: ${значение из поля ввода}. Рядом с ним должна быть кнопка с крестиком (X). Значение внутри поля ввода окрашивается в зеленый цвет.
// При нажатии на Х - span с текстом и кнопка X должны быть удалены. Значение, введенное в поле ввода, обнуляется.
// Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода красной рамкой, под полем выводить фразу - Please enter correct price. span со значением при этом не создается.
//
//
// В папке img лежат примеры реализации поля ввода и создающегося span.
//
function inputFocus () {
    let input = document.getElementById('price');
    input.style.border = '1px solid green'
    input.style.outline = 'none'

    if (input.classList.contains('input-fix-color')){
        input.classList.remove('input-fix-color');
    }
}
function inputBlur () {
    let input = document.getElementById('price');
    input.style.border = '1px solid grey'

    let span = document.createElement('span');

    let removeBtn = document.createElement('button');

    if (input.value >= 0){
        //span + value
        span.innerText = `Current price:  ${input.value}`;
        span.appendChild(removeBtn);
        span.classList.add('span_js')
        document.getElementById('label').before(span);

        //value color
        input.classList.add('input-fix-color');

        //button on click

        removeBtn.classList.add('span_btn')
        removeBtn.innerText = 'X'
        removeBtn.type = 'button';
        removeBtn.innerText = 'X'
        removeBtn.onclick = () => {
            removeBtn.classList.toggle('hidden')
            span.classList.toggle('hidden');
            input.value = ''
        }
    }else if (input.value < 0){
        let divError = document.createElement('div');
        divError.innerHTML = 'Please enter correct price.';
        document.getElementById('label').after(divError);
        input.style.borderColor = 'red'
    }
}