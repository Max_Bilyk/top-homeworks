// alert('Отличие setTimeout от setInterval в том что setInterval запускается переодически через указаный интервал времени, а setTimeout запускается 1 раз');
// alert('Если передать в timeout 0 задержку то вызов функции планируется настолько быстро, насколько это возможно, но setTimeout будет вызываться только после завершения выполнения текущего кода. То есть вызов setTimeout будет запланирован сразу после выполнения текущего кода')
// alert('')
let left = 0;
let timer;
function autoSlider () {
    timer = setTimeout(() => {
        let slider = document.getElementById('slider');
        left = left - 400;
        if (left < -1200){
            left = 0;
        }
        slider.style.left = left + 'px';
        autoSlider();
    }, 1000 );
}
autoSlider();
//
let stopSliderBtn = document.getElementById('stop')
stopSliderBtn.addEventListener('click', stopSlide);
function stopSlide () {
    if (continueSliderBtn.disabled){
        continueSliderBtn.disabled = false;
    }
    clearTimeout(timer);
}
let continueSliderBtn = document.getElementById('continue')
continueSliderBtn.addEventListener('click', continueSlide);
function continueSlide () {
    continueSliderBtn.disabled = true;
    autoSlider();
}


//3
// let images = document.querySelectorAll('.infinity-slider img');
// let current = 0;
//
// function slider () {
//     for (let i = 0; i < images.length; i++) {
//         images[i].classList.add('opacity0')
//     }
//     images[current].classList.remove('opacity0');
// }
// slider();
// // document.querySelector('.infinity-slider').onclick = slider;
//
// document.querySelector('.btn-infinity-up').onclick = function () {
//     if (current - 1 === -1){
//         current = images.length - 1;
//     }else {
//         current--;
//     }
//     slider();
// };
// document.querySelector('.btn-infinity-down').onclick = function () {
//     if (current + 1 === images.length){
//         current = 0;
//     }else {
//         current++;
//     }
//     slider();
// };
// let imgContent = document.querySelectorAll('.image-to-show');
// let currentImg = 0;
// let slider = setInterval(imgSlider, 1000);
// function imgSlider() {
//     imgContent[currentImg].classList.remove('active-image');
//     currentImg = (currentImg + 1) % imgContent.length;
//     imgContent[currentImg].classList.add('active-image');
// }
