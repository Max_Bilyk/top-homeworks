//    < === First block with tabs === >   \\
//   <-- 1 variant using javascript -->   \\
// document.querySelectorAll('.tab__link').forEach(item => {
//     item.addEventListener('click', function (e) {
//         e.preventDefault();
//         let eventTarget = e.target;
//         let id = eventTarget.getAttribute('href').replace('#', '')
//
//         document.querySelectorAll('.tab__link').forEach(item => {
//             item.classList.remove('active-tab');
//         });
//         document.querySelectorAll('.services__content').forEach(item => {
//             item.classList.remove('active-services__content');
//         });
//
//         item.classList.add('active-tab');
//         document.getElementById(id).classList.add('active-services__content');
//     })
// });

//   < === First block with tabs === >   \\
//   <-- 2 variant using query -->   \\

//   < === Get links, reset theirs default browser settings and stop bubbling === >   \\
$('.item__link').click(function (e) {
    e.stopPropagation();
    e.preventDefault();

    //   < === Remove active class from tab links and tab content === >   \\
    $('.item__link').removeClass('active-link');
    $('.services-tab-content').removeClass('active-tab-content');

    //   < === For THIS clicked link add active class, then get THIS link(ссылку) href and link (связываю) the link(ссылку) by ID === >   //
    $(this).addClass('active-link');
    $($(this).attr('href')).addClass('active-tab-content');
});


//   < === Amazing work section === >   \\
//   < === Create variables === >   \\
const graph = "graphic-design";
const web = "web-design";
const landing = "landing-pages";
const wordpress = "wordpress";
let content = $(".list-content");
let loadBtnStatus = false;
let currentClass = false;

//   < === Create hover effect for each theme === >   \\
const imageContentWithHoverEffect = {
    graph: "<div class='list-hover'> <div class='hover__links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='link__title'><p>cool design</p><p>graphic design</p></div></div>",
    web: "<div class='list-hover'> <div class='hover__links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='link__title'><p>creative design</p><p>web design</p></div></div>",
    land: "<div class='list-hover'> <div class='hover__links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='link__title'><p>amazing pages</p><p>landing pages</p></div></div>",
    word: "<div class='list-hover'> <div class='hover__links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='link__title'><p>yeah wordpress</p><p>wordpress</p></div></div>"
};

//   < === Adding images === >   \\
for (let i = 1; i <= 3; i++) {
    $(content).append(`<div class='list-img ${graph}'><img src='images/filteredIMG/${graph}/${graph}${i}.jpg' alt='web'></div>`);
    $(content).append(`<div class='list-img ${web}'><img src='images/filteredIMG/${web}/${web}${i}.jpg' alt='web'></div>`);
    $(content).append(`<div class='list-img ${landing}'><img src='images/filteredIMG/${landing}/${landing}${i}.jpg' alt='web'></div>`);
    $(content).append(`<div class='list-img ${wordpress}'><img src='images/filteredIMG/${wordpress}/${wordpress}${i}.jpg' alt='web'></div>`);
}
//   < === Add hover effect === >   \\
function getImgHoverEffect () {
    $(".graphic-design>img").before(imageContentWithHoverEffect.graph);
    $(".web-design>img").before(imageContentWithHoverEffect.web);
    $(".landing-pages>img").before(imageContentWithHoverEffect.land);
    $(".wordpress>img").before(imageContentWithHoverEffect.word);
}

//   < === Filter === >   \\
$(".list__item").click((event) => {
    //   < === For each item with class 'list__item' remove active class, and for THIS add active class === >   \\
    $(".list__item").removeClass("active__item");
    $(event.target).addClass("active__item");
    //   < === Then I assign in variable currentClass THIS list__item data-attr ===>   \\
    //   < === And if data-attr === appended item`s class in first loop, appended item === display: block === >   \\
    currentClass = $(event.target).attr('data-tab-theme');
    if (currentClass !== "all") {
        $(`.${currentClass}`).fadeIn("fast");
        $(".list-img").not(`.${currentClass}`).css("display", "none");
    }
    else {
        $(".list-img").css("display", "block");
    }
});

//   <=== Load more button === >   \\
$(".image-load").click(() => {
    $(".work-section>.loader").css("display", "flex");
    setTimeout(() => {
        $(".work-section>.loader").css("display", "none");
        if (!loadBtnStatus) {
            for (let j = 4; j <= 6; j++) {
                $(content).append(`<div class='list-img ${graph}'><img src='images/filteredIMG/${graph}/${graph}${j}.jpg' alt='web'></div>`);
                $(content).append(`<div class='list-img ${web}'><img src='images/filteredIMG/${web}/${web}${j}.jpg' alt='web'></div>`);
                $(content).append(`<div class='list-img ${landing}'><img src='images/filteredIMG/${landing}/${landing}${j}.jpg' alt='web'></div>`);
                $(content).append(`<div class='list-img ${wordpress}'><img src='images/filteredIMG/${wordpress}/${wordpress}${j}.jpg' alt='web'></div>`);
                loadBtnStatus = true;
            }
        } else {
            for (let k = 7; k <= 9; k++) {
                $(content).append(`<div class='list-img ${graph}'><img src='images/filteredIMG/${graph}/${graph}${k}.jpg' alt='web'></div>`);
                $(content).append(`<div class='list-img ${web}'><img src='images/filteredIMG/${web}/${web}${k}.jpg' alt='web'></div>`);
                $(content).append(`<div class='list-img ${landing}'><img src='images/filteredIMG/${landing}/${landing}${k}.jpg' alt='web'></div>`);
                $(content).append(`<div class='list-img ${wordpress}'><img src='images/filteredIMG/${wordpress}/${wordpress}${k}.jpg' alt='web'></div>`);
                $(".image-load").hide();
            }
        }
        if (currentClass !== "all" && currentClass !== false) {
            $(`.${currentClass}`).css("display", "block");
            $(".list-img").not(`.${currentClass}`).css("display", "none");
        }
        getImgHoverEffect();
    }, 2000);
});
getImgHoverEffect();

//   < === Slider === >   \\
let slideIndex = 1;
function btnSlide (n){
    showSlides(slideIndex += n);
}
function currentSlide (n) {
    showSlides(slideIndex = n);
}
function showSlides (n) {
    let i;
    let slides = document.getElementsByClassName("slider__content");
    let dots = document.getElementsByClassName("dot");

    if (n > slides.length){
        slideIndex = 1;
    }
    if (n < 1){
        slideIndex = slides.length;
    }

    for (i = 0; i < slides.length; i++){
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++){
        dots[i].className = dots[i].className.replace(" active-dot", "");
    }
    slides[slideIndex-1].style.display = "flex";
    dots[slideIndex-1].className += " active-dot";
}
showSlides(slideIndex);

//   < === Masonry === >   //

let grid = $('.grid');

$(".masonry-load-btn").click(function () {

    $(".masonry-gallery >.loader").css("display", "flex");

    setTimeout(function () {
        $(".masonry-gallery >.loader").css("display", "none");
        $(".masonry-load-btn").remove();

        for (let i = 1; i < 8; i++) {
            $('.grid').append(`<div class="grid-item grid-hide"><div class="grid-hover"><a href="#"><i class="fas fa-search"></i></a><a href="#"><i class="fas fa-expand"></i></a></div><img src="images/gallery/gallery-img${i}.png" alt=""></div>`)
        }

        $(grid).masonry('reloadItems');
        $(grid).masonry('layout');
        window.setTimeout(function () {
            $('.grid').masonry({
                itemSelector: '.grid-item',
                columnWidth: 25,
                gutter: 10
            });
        }, 500);
    }, 2000);
});

$(grid).masonry({
    itemSelector: '.grid-item',
    columnWidth: 25,
    gutter: 10
});