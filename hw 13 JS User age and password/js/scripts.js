// Технические требования:
//
//     Возьмите выполненное домашнее задание номер 4 (созданная вами функция createNewUser()) и дополните ее следующим функционалом:
//
//     При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.
//     Создать метод getAge() который будет возвращать сколько пользователю лет.
//     Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре,
//     соединенную с фамилией (в нижнем регистре) и годом рождения.(например, Ivan Kravchuk 13.03.1992 → IKravchuk1992).
//
//
// Вывести в консоль результат работы функции createNewUser(), а также функций getAge() и getPassword() созданного объекта.
function createNewUser() {
    let userBirth = prompt('Enter your birthday in dd.mm.yyyy format', '04.06.2006')
    let userFirstName = prompt('Enter your first name');
    let userLastName = prompt('Enter your last name');

    return newUser = {
        birthday: userBirth,
        first_name: userFirstName,
        last_name: userLastName,
        // getLogin: function () {
        //     return this.first_name.charAt(0).toLowerCase() + this.last_name.toLowerCase();
        // }
        getAge: function () {
            let now = new Date();
            let currentYear = now.getFullYear();

            let userDate = +this.birthday.substring(0, 2);
            let userMonth = +this.birthday.substring(3, 5);
            let userYear = +this.birthday.substring(6, 10);

            let userBirthDate = new Date(userYear, userMonth-1, userDate-1);
            let birthYear = userBirthDate.getFullYear();
            return currentYear - birthYear;
        },
        getPassword: function () {
            return this.first_name.charAt(0).toUpperCase() + this.last_name.toLowerCase() + this.birthday.substring(6,10);
        }
    }
}
console.log(createNewUser());
//alert ДЛЯ СЕБЯ!!!
alert('Look at the console');

//---------------------------------
console.log((`Your age ---> ${newUser.getAge()}`));
console.log(`Your login name ---> ${newUser.getPassword()}`);

