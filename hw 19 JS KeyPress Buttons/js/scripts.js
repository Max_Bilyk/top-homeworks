// Каждая кнопка содержит в себе название клавиши на клавиатуре
// По нажатию указанных клавиш - та кнопка, на которой написана эта буква, должна окрашиваться в синий цвет.
// При этом, если какая-то другая буква уже ранее была окрашена в синий цвет - она становится черной.
// Например по нажатию Enter первая кнопка окрашивается в синий цвет.
// Далее, пользователь нажимает S, и кнопка S окрашивается в синий цвет, а кнопка Enter опять становится черной.
function btnChangeColor() {
    const button = document.querySelectorAll('.btn');
    document.onkeypress = function (e) {
        button.forEach(item => {
            if (e.key.toLowerCase() === item.textContent.toLowerCase()) {
                return item.classList.add('active_btn');
            }
            item.classList.remove('active_btn');
        });
    };
}
btnChangeColor();
