/**
 * @param {Object} student
 * @param {String} studentName
 * @param {String} studentLastName
 * @param {String} studentSubject
 * @param {Number} studentMark
 * @param {Boolean} badMarks
 **/

let student = {
    'name': {},
    'last name': {},
    //табель переименовал в timesheet потому что подказывает что такого слова нету и немного подчёркивает, но код так же работает, просто так на глаз приятней
    'timesheet': {},
}

let studentName = prompt('Введите ваше имя');
let studentLastName = prompt('Введите вашу фамилию');

student.name = studentName;
student["last name"] = studentLastName;

let studentSubject = prompt("Введите название предмета");
let studentMark = +prompt("Введите оценку по предмету");

while (studentSubject) {
    student.timesheet[studentSubject] = studentMark;
    studentSubject = prompt("Введите название предмета");
    studentMark = +prompt("Введите оценку по предмету");
}

let badMarks = false

for (let key in student.timesheet){
    if (student.timesheet[key] < 4){
        console.log(`Студент ${studentName} ${studentLastName} отправлен на пересдачу`)
        badMarks = true;
        break;
    }
}

if (!badMarks){
    console.log(`Студент ${studentName} ${studentLastName} переведен на следующий курс.`)

    let averageMark = 0;
    let averageCounter = 0;

    for (let key in student.timesheet){
        averageCounter++
        averageMark += student.timesheet[key];
    }

    averageMark = averageMark / averageCounter;

    if (averageMark > 7) {
        console.log(`Студенту ${studentName} ${studentLastName} назначена стипендия`);
    }
}
console.log(student.timesheet);