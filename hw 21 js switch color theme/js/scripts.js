let themeToggleBtn = document.querySelector('#theme-toggle');

function initialState(themeName) {
    localStorage.setItem('theme', themeName);
    document.getElementById('wrapper').className = themeName;
}

function switchColorTheme() {

    if (localStorage.getItem('theme') === 'dark-theme'){
        initialState('light-theme');
    }else {
        initialState('dark-theme')
    }
}
themeToggleBtn.addEventListener('click', switchColorTheme);

document.addEventListener('DOMContentLoaded', function (){
    initialState(localStorage.getItem('theme'));
});

// let themeToggleBtn = document.getElementById('theme-toggle');
// themeToggleBtn.addEventListener('click', switchThemeColor);

// function toLocalStorage () {
//
//
// }

// function switchThemeColor() {
//     if (document.documentElement.hasAttribute('theme')){
//         document.documentElement.removeAttribute('theme');
//     }else {
//         document.documentElement.setAttribute('theme', 'color-theme');
//     }
// }
// localStorage.setItem('color-theme', switchThemeColor);